<x-layout>
  <x-slot name="style">
    <style type="text/tailwindcss">
     @layer components {
       .subscribe-btn {
	 @apply rounded-full ml-3 py-3 px-5 uppercase text-xs font-semibold text-white;
       }
     }
    </style>
  </x-slot>
  
  <x-slot name="content">
      <section>
	<x-nav />
      <!-- Header Title Section -->
      <header class="max-w-4xl mx-auto mt-20 text-center">
	<div class="max-w-xl mx-auto">
	  <h1 class="text-4xl">Letest <span class="text-blue-500 ">Laravel From Scratch</span> News</h1>
	  <h2 class="inline-flex mt-2">
	    By Lary Laracore
	    <img alt="Head of Laracast mascot" src="/images/lary-head.svg"/>
	  </h2>
	  <p class="text-sm mt-20">
	    Another Year. Another Update. New Blog with new News. I'am
	    going to keep you guyes up to speed with what's going on.
	  </p>
	</div>
	<!-- Drop Down Section -->
	<div class="space-y-2 md:space-x-4 mt-2 flex flex-col md:block">
	  <!-- Category Drop Down -->
	  <span class="bg-gray-100 md:inline-block rounded-xl">
	    <select id="category" name="category"
		    class="bg-transparent p-2 text-sm font-semibold w-11/12">
	      <option value="category" disabled selected>Category</option>
	      <option value="laravel8">Laravel 8</option>
	      <option value="tailwind">Tailwind CSS</option>
	    </select>
	  </span>
	  <!-- Filter Drop Down -->
	  <span class="bg-gray-100 inline-block rounded-xl">
	    <select id="category" name="category" class="bg-transparent p-2 text-sm font-semibold w-11/12">
	      <option value="category" disabled selected>Other Filters</option>
	      <option value="laravel8">Laravel 8</option>
	      <option value="tailwind">Tailwind CSS</option>
	    </select>
	  </span>
	  <!-- Search Form -->
	  <span class="bg-gray-100 inline-block rounded-xl py-2 pl-4">
	    <form method="GET" action="#" class="flex">
	      <label for="search"><i class="fas fa-search mx-2"></i></label>
	      <input class="bg-transparent placeholder-black font-semibold text-sm focus-within:outline-none"
		     id="search" name="search" type="text" placeholder="Find Something?"/>
	    </form>
	  </span>
	</div>
      </header>
      <!-- Articles Section -->
      <main class="mt-20 max-w-6xl mx-auto space-y-6">
	<x-post-card />
	<div class="grid lg:grid-cols-2">
	  <x-post-mini-card />
	  <x-post-mini-card />
	</div>
	<div class="grid lg:grid-cols-3">
	  <x-post-mini-card />
	  <x-post-mini-card />
	  <x-post-mini-card />
	</div>
      </main>
      <!-- Main Footer -->
      <x-footer />
    </section>
  </x-slot>
</x-layout>

<article class="transition-colors durations-300 hover:bg-gray-100 rounded-xl border border-black 
		border-opacity-0 hover:border-opacity-5 ">
  <div class="lg:flex justify-between px-5 py-6">
    <div class="flex-1 lg:mr-8">
      <img alt="" src="/images/illustration-1.png" class="rounded-xl" />
    </div>
    <div class="flex-1 flex flex-col justify-between pt-4 lg:pt-0">
      <header>
	<div class="space-x-2">
	  <span class="border border-blue-400 text-blue-400 rounded-full font-semibold py-1 px-4"
	  style="font-size: 10px;">Techniques</span>
	  <span class="border border-red-400 text-red-400 rounded-full font-semibold py-1 px-4"
		       style="font-size: 10px;">Updates</span>
	</div>
	<div class="mt-3">
	  <h1 class="text-3xl">
	    This is a Big Title and It's Looks Great on Two or Even Three Lines. Woohoo!
	  </h1>
	  <span class="text-gray-400 text-xs block mt-2">
	    Published <time>1 day ago</time>
	  </span>
	</div>
      </header>
      <main class="text-sm"> 
	<p class="lg:my-1 my-4">
	  Fusce id velit ut tortor pretium viverra suspendisse
	  potenti nullam ac tortor vitae purus faucibus ornare
	  suspendisse sed? Euismod in pellentesque massa
	  placerat duis ultricies lacus sed turpis tincidunt
	  id. Elit duis tristique sollicitudin nibh sit amet
	  commodo nulla facilisi nullam vehicula ipsum a arcu!
	</p>
	<p class="my-1">
	  Nunc eget lorem dolor, sed viverra ipsum nunc aliquet bibendum.
	</p>
      </main>
      <footer class="flex items-center justify-between">
	<div class="flex items-center text-sm">
	  <img alt="" src="/images/lary-avatar.svg"/>
	  <div class="ml-2">
	    <h5 class="font-semibold">Lary Laracore</h5>
	    <h6>Mascot at Laracast</h6>
	  </div>
	</div>
	<div class="hidden md:block">
	  <a href="" class="text-xs font-semibold bg-gray-200 rounded-full py-2 px-6">Read More</a>
	</div>
      </footer>
    </div>
  </div>
</article>

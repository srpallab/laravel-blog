<footer class="mt-20 bg-gray-100 border border-black border-opacity-5 rounded-xl text-center py-15 px-10">
  <div class="flex flex-col items-center p-8">
    <img alt="Lary Newsletter Icon" src="/images/lary-newsletter-icon.svg"/>
    <h1 class="text-3xl">Stay in Touch with Latest Posts.</h1>
    <p class="text-sm">Promise to keep the inbox clean. No bugs.</p>
    <div class="mt-8 bg-gray-200 rounded-full ">
      <form action="GET" class="flex items-center">
	<label for="email" class="md:mx-4 mr-1 ml-3">
	  <i class="fas fa-envelope text-gray-400"></i>
	</label>
	<input class="bg-transparent focus-within:outline-none" id="email" name="email"
	type="email" placeholder="Your Email Address"/>
	<button class="bg-blue-500 uppercase text-white font-semibold md:text-sm text-xs 
		       rounded-full md:py-3 md:px-6 px-1 py-2 hover:bg-blue-600
		       transition-colors durations-300">
	  Subscribe
	</button>
      </form>
    </div>
  </div>
</footer>

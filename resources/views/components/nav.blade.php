<nav class="flex flex-col md:flex-row justify-between items-center">
  <div>
    <a href="#">
      <img alt="Laracast Logo" src="/images/logo.svg" width="165" height="16">
    </a>
  </div>
  <div class="mt-6 md:mt-0">
    <a href="/" class="uppercase text-xs font-bold ">Home Page</a>
    <a href="" class="subscribe-btn transition-colors durations-300 bg-blue-500 hover:bg-blue-600">
      Subscribe for Updates
    </a>
  </div>
</nav>
